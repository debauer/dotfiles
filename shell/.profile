
export BEMENU_BACKEND=wayland
export CLUTTER_BACKEND=wayland
export GDK_BACKEND=wayland,x11

export QT_QPA_PLATFORM=wayland

export SDL_VIDEODRIVER=wayland
export MOZ_ENABLE_WAYLAND=1
export _JAVA_AWT_WM_NONREPARENTING=1

export XDG_CURRENT_DESKTOP=sway
export XDG_SESSION_DESKTOP=sway

export _JAVA_AWT_WM_NONREPARENTING=1
. "$HOME/.cargo/env"

. "$HOME/.atuin/bin/env"
